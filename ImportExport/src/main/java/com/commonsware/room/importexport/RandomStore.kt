/*
  Copyright (c) 2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.importexport

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import java.time.Instant

data class Summary(
  val count: Int,
  val oldestTimestamp: Instant? = null
)

@Dao
interface RandomStore {
  @Insert
  suspend fun insert(entities: List<RandomEntity>)

  @Query("SELECT COUNT(*) as count, MIN(timestamp) as oldestTimestamp FROM randomStuff")
  suspend fun summarize(): Summary
}