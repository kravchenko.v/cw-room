/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.todo.repo

import org.threeten.bp.Instant
import java.util.*

data class ToDoModel(
  val description: String,
  val id: String = UUID.randomUUID().toString(),
  val isCompleted: Boolean = false,
  val notes: String = "",
  val createdOn: Instant = Instant.now()
) {
}