/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.todo.repo

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.commonsware.todo.RxSchedulerRule
import com.jraska.livedata.test
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ToDoDatabaseRxTest {
  @get:Rule
  val instantExecutorRule = InstantTaskExecutorRule()

  @get:Rule
  val rxSchedulerRule = RxSchedulerRule.test()

  private val context =
    InstrumentationRegistry.getInstrumentation().targetContext
  private val db = ToDoDatabase.newTestInstance(context)
  private val underTest = db.todoStore()

  @Test
  fun basicCRUD() {
    val entities = arrayOf(
      ToDoEntity(description = "this is a test", isCompleted = true),
      ToDoEntity(description = "this is another test"),
      ToDoEntity(description = "this is... wait for it... yet another test")
    )

    underTest.save(*entities).test()
    rxSchedulerRule.scheduler.triggerActions()
    underTest.all().test().assertValue { it.containsAll(entities.toList()) }

    underTest.delete(*entities).test()
    rxSchedulerRule.scheduler.triggerActions()
    underTest.all().test().assertValue { it.isEmpty() }
  }
}