/*
  Copyright (c) 2021 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.todo.ui

import androidx.lifecycle.*
import com.commonsware.todo.repo.ToDoModel
import com.commonsware.todo.repo.ToDoRepository
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

data class SingleModelViewState(
  val item: ToDoModel? = null,
  val loginRequired: Boolean = false
)

class SingleModelMotor(
  private val repo: ToDoRepository,
  modelId: String?
) : ViewModel() {
  val states: LiveData<SingleModelViewState> = if (repo.isReady()) {
    repo.find(modelId).map { SingleModelViewState(it) }.asLiveData()
  } else {
    MutableLiveData<SingleModelViewState>(SingleModelViewState(loginRequired = true))
  }

  fun save(model: ToDoModel) {
    viewModelScope.launch {
      repo.save(model)
    }
  }

  fun delete(model: ToDoModel) {
    viewModelScope.launch {
      repo.delete(model)
    }
  }
}
