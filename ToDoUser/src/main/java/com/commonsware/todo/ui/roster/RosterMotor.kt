/*
  Copyright (c) 2021 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.todo.ui.roster

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.commonsware.todo.repo.ToDoModel
import com.commonsware.todo.repo.ToDoRepository
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

data class RosterViewState(
  val items: List<ToDoModel> = listOf(),
  val authRequired: Boolean = false
)

class RosterMotor(
  private val context: Context,
  private val repo: ToDoRepository
) : ViewModel() {
  private val _states =
    MutableLiveData<RosterViewState>(RosterViewState(authRequired = true))
  val states: LiveData<RosterViewState> = _states

  fun open(passphrase: String) {
    viewModelScope.launch {
      if (repo.openDatabase(context, passphrase)) {
        repo.items().collect { _states.value = RosterViewState(items = it) }
      } else {
        _states.value = RosterViewState(authRequired = true)
      }
    }
  }

  fun save(model: ToDoModel) {
    viewModelScope.launch {
      repo.save(model)
    }
  }
}