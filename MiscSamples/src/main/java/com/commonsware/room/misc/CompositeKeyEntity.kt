/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.misc

import androidx.room.Dao
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.Query

@Entity(tableName = "compositeKey", primaryKeys = ["id", "version"])
data class CompositeKeyEntity(
  val id: String,
  val title: String,
  val text: String = "",
  val version: Int = 1
) {
  @Dao
  interface Store {
    @Query("SELECT * FROM compositeKey")
    fun loadAll(): List<CompositeKeyEntity>

    @Query("SELECT * FROM compositeKey where id = :id AND version = :version")
    fun findByPrimaryKey(id: String, version: Int): CompositeKeyEntity

    @Insert
    fun insert(entity: CompositeKeyEntity)
  }
}