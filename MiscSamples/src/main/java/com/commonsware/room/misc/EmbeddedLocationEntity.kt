/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.misc

import android.location.Location
import android.util.JsonReader
import android.util.JsonWriter
import androidx.room.*
import org.threeten.bp.Instant
import java.io.StringReader
import java.io.StringWriter

data class LocationColumns(
  val latitude: Double,
  val longitude: Double
) {
  constructor(loc: Location) : this(loc.latitude, loc.longitude)
}

@Entity(tableName = "embedded")
data class EmbeddedLocationEntity(
  @PrimaryKey(autoGenerate = true)
  val id: Long = 0,
  val name: String,
  @Embedded
  val location: LocationColumns
) {
  @Dao
  interface Store {
    @Query("SELECT * FROM embedded")
    fun loadAll(): List<EmbeddedLocationEntity>

    @Insert
    fun insert(entity: EmbeddedLocationEntity)
  }
}
