/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.misc

import android.location.Location
import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import com.natpryce.hamkrest.hasSize
import com.natpryce.hamkrest.isEmpty
import org.junit.Test
import org.junit.runner.RunWith
import org.threeten.bp.Instant

private val TEST_LOCATION = Location(null as String?).apply {
  latitude = 40.711472
  longitude = -74.012725
}
private val TEST_TAGS = setOf("test1", "test2", "test3")

@RunWith(AndroidJUnit4::class)
class TransmogrifiyingEntityTest {
  private val db = Room.inMemoryDatabaseBuilder(
    InstrumentationRegistry.getInstrumentation().targetContext,
    MiscDatabase::class.java
  )
    .build()
  private val underTest = db.transmogrified()

  @Test
  fun queryDisplayModels() {
    assertThat(underTest.loadAll(), isEmpty)

    val now = Instant.now()
    val entity = TransmogrifyingEntity(
      creationTime = now,
      location = TEST_LOCATION,
      tags = TEST_TAGS
    )

    underTest.insert(entity)

    underTest.loadAll().let {
      assertThat(it, hasSize(equalTo(1)))
      assert(it[0].creationTime == entity.creationTime)
      assert(it[0].location.latitude == entity.location.latitude)
      assert(it[0].location.longitude == entity.location.longitude)
      assert(it[0].tags == entity.tags)
    }
  }
}
