/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.misc

import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import com.natpryce.hamkrest.hasSize
import com.natpryce.hamkrest.isEmpty

import org.junit.Test
import org.junit.runner.RunWith

import java.util.*

@RunWith(AndroidJUnit4::class)
class IndexedEntityTest {
  private val db = Room.inMemoryDatabaseBuilder(
    InstrumentationRegistry.getInstrumentation().targetContext,
    MiscDatabase::class.java
  )
    .build()
  private val underTest = db.indexed()

  @Test
  fun queryByCategory() {
    assertThat(underTest.loadAll(), isEmpty)

    val funStuff = IndexedEntity(
      id = UUID.randomUUID().toString(),
      title = "This is fun!",
      category = "fun-stuff",
      text = "words words words"
    )
    val notAsFunStuff = IndexedEntity(
      id = UUID.randomUUID().toString(),
      title = "Gloom, despair, and agony on me",
      category = "un-fun-stuff"
    )

    underTest.insert(funStuff, notAsFunStuff)

    underTest.loadAllForCategory("fun-stuff").let {
      assertThat(it, hasSize(equalTo(1)))
      assert(it[0] == funStuff)
    }
  }
}
