package com.commonsware.room.misc

import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import com.natpryce.hamkrest.isEmpty

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import java.util.*

@RunWith(AndroidJUnit4::class)
class DefaultValueEntityTest {
  private val db = Room.inMemoryDatabaseBuilder(
    InstrumentationRegistry.getInstrumentation().targetContext,
    MiscDatabase::class.java
  )
    .build()
  private val underTest = db.defaultValue()

  @Test
  fun defaultsIgnored() {
    assertThat(underTest.loadAll(), isEmpty)

    val original = DefaultValueEntity(
      id = UUID.randomUUID().toString(),
      title = "This is a title. No, really, it is!"
    )

    underTest.insert(original)

    val retrieved = underTest.findByPrimaryKey(original.id)

    assertThat(retrieved.id, equalTo(original.id))
    assertThat(retrieved.title, equalTo(original.title))
    assertThat(retrieved.text, equalTo(null as String?))
    assertThat(retrieved.version, equalTo(1))
  }

  @Test
  fun insertByQuery() {
    assertThat(underTest.loadAll(), isEmpty)

    val id = UUID.randomUUID().toString()
    val title = "a really great title"

    underTest.insertByQuery(id, title)

    val retrieved = underTest.findByPrimaryKey(id)

    assertThat(retrieved.id, equalTo(id))
    assertThat(retrieved.title, equalTo(title))
    assertThat(retrieved.text, equalTo("something"))
    assertThat(retrieved.version, equalTo(123))
  }

  @Test
  fun insertPartial() {
    assertThat(underTest.loadAll(), isEmpty)

    val original = PartialDefaultValue(
      id = UUID.randomUUID().toString(),
      title = "This is a title. No, really, it is!"
    )

    underTest.insertPartial(original)

    val retrieved = underTest.findByPrimaryKey(original.id)

    assertThat(retrieved.id, equalTo(original.id))
    assertThat(retrieved.title, equalTo(original.title))
    assertThat(retrieved.text, equalTo("something"))
    assertThat(retrieved.version, equalTo(123))
  }
}
