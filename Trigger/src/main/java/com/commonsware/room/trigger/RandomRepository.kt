/*
  Copyright (c) 2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.trigger

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.withContext
import java.util.concurrent.Executors
import kotlin.random.Random

class RandomRepository(
  private val db: RandomDatabase,
  private val appScope: CoroutineScope
) {
  private val dispatcher =
    Executors.newSingleThreadExecutor().asCoroutineDispatcher()

  fun summarize() = db.randomStore().summarize()

  suspend fun populate() {
    withContext(dispatcher + appScope.coroutineContext) {
      val count = Random.nextInt(100) + 1

      db.randomStore().insert((1..count).map { RandomEntity(0) })
    }
  }

  suspend fun count() = db.countStore().getCurrent()
}